# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
 hash = Hash.new(0)
 str.split(" ").each do |word|
   hash[word]= word.length
 end
 hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.key(hash.values.max)
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)
  word.chars.each do |let|
    hash[let] = word.count(let)
  end
  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each {|elem| hash[elem]= 0}
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)
  numbers.each do |int|
    if int % 2 == 0
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end
  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  counts_of_vowels = Hash.new(0)
  string.chars.each do |let|
    if vowels.include?(let)
      counts_of_vowels[let] += 1
    end

 end
 largest_val = counts_of_vowels.values.max
 max_hash = counts_of_vowels.select { |k, v| v == largest_val }.keys
 max_hash
 max_hash.sort_by {|k, v| k}[0]

end
# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  hash = Hash.new(0)
  hash = students.select {|k, v| v >= 7}

  birthday_names = []
  key_names = hash.keys
  key_names.each_index do |idx1|
    ((idx1 + 1)...key_names.length).each do |idx2|
      birthday_names << [ key_names[idx1], key_names[idx2] ]
    end
  end
  birthday_names
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  population_count = Hash.new(0)
  specimens.each { |spec| population_count[spec] += 1 }
  population_size = population_count.sort_by{ |k, v| v}
  (population_count.keys.length ** 2)*(population_size[0][1]/population_size[-1][1])
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)
  vandalized_count.each do |k, v|
    if v > normal_count[k]
      return false
    end
  end
  true
end

def character_count(str)
  count = Hash.new(0)
  str.downcase.chars.each { |char| count[char] += 1 }

  count
end
